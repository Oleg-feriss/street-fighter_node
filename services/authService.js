const UserService = require('./userService');

class AuthService {
    login(userData) {
        const { login, password } = userData; 
        const user = UserService.getUser(user => (
            user.login === login && user.password === password
        ));
        if(!user) {
            throw Error('User not found');
        }
        return user;
    }
}

module.exports = new AuthService();