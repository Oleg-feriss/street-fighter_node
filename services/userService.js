const { UserRepository } = require('../repositories/userRepository');

class UserService {
  getAllUsers() {
    const allUsers = UserRepository.getAll();
    if (!allUsers) {
      throw ('Users not found');
    }
    return allUsers;
  }

  getOneUser(search) {
    const user = UserRepository.getOne(search);
    if (!user) {
      throw ('User not found');
    }
    return user;
  }

  createUser(data) {
    const newUser = UserRepository.create(data);
    if (!newUser) {
      throw ('User can not be created');
    }
    return newUser;
  }

  updateUser(id, data) {
    const user = this.getOneUser(user => user.id === id);
    const updatedUser = {...user};
    
    for (const key in updatedUser) {
      if (key in data) {
        updatedUser[key] = data[key];
      };
    };
    
    const res = UserRepository.update(id, updatedUser);
    if (!res) {
      throw ('User can not be updated');
    }
    return res;
  }

  deleteUser(id) {
    const removedUser = UserRepository.delete(id);
    if (!removedUser) {
      throw ('User can not be deleted');
    }
    return removedUser;
  }
}

module.exports = new UserService();