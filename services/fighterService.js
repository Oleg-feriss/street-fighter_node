const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {

    getFighters() {
        const fighters = FighterRepository.getAll();
        if (!fighters) {
            throw ('Fighters not found');
        }
        return fighters;
    }

    getFighter(search) {
        const fighter = FighterRepository.getOne(search);
        if (!fighter) {
            throw ('Fighter not found');
        }
        return fighter;
    }

    createFighter(data) {
        const newFighter = FighterRepository.create(data);
        if (!newFighter) {
            throw ('Fighter can not be created');
        }
        return newFighter;
    }

    updateFighter(id, data) {
        const fighter = this.getFighter(fighter => fighter.id === id);
        const updatedFighter = {...fighter}; 
        
        for (const key in updatedFighter) {
            if (key in data) {
                updatedFighter[key] = data[key];
            };
        };

        const res = FighterRepository.update(id, updatedFighter);
        if (!res) {
            throw ('Fighter can not be updated');
        }
        return res;
    }

    deleteFighter(id) {
        const removedFighter = FighterRepository.delete(id);
        if (!removedFighter) {
            throw ('Fighter can not be deleted');
        }
        return removedFighter;
    }
}

module.exports = new FighterService();