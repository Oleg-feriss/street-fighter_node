const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    const { id, ...rest } = req.body;
    
    if(!req.body.health) {req.body.health = 100};

    if (id || !isBodyValid(rest)) {
        res.status(400);
        res.err = 'Fighter entity to create is not valid';
    };
    
    next();
};

const updateFighterValid = (req, res, next) => {
    const { id, ...rest } = req.body;
    
    if (id || !isBodyValid(rest) || isEmptyObject(rest)) {
        res.status(400);
        res.err = 'Fighter entity to update is not valid';
    };

    next();
};

const isModelComplete = (data) => {
    for (const key in fighter) {
        if (key !== 'id' && !data[key]) {
            return false;
        }
    };
    return true;
};

const isBodyValid = (data) => {
    for (const key in data) {
        if (!isValid(key, data) || !isPrimitive(data[key])) {
            return false;
        }
    }
    return true;
};

const isEmptyObject = (data) => {
    for (const key in data) {
        if (data.hasOwnProperty(key)) {
            return false;
        }
    }
    return true;
}

const isValid = (key, data) => {
    switch(key) {
        case 'name': return isNameValid(data[key]);
        case 'health': return isHealthValid(data[key]);
        case 'power': return isPowerValid(data[key]);
        case 'defense': return isDefenseValid(data[key]);
        default: return false;
    };
};

const isNameValid = (name) => (
    isUnique(fighter => fighter.name === name)
);

const isUnique = (search) => {
    try {
        const fighter = FighterService.getFighter(search);
        return false;
    } catch (err) {
        return true;
    };
};

const isPowerValid = (power) => (
    (power == parseInt(power)) && power <= 100 && power > 0
);

const isHealthValid = (health) => (
    (health == parseInt(health)) && health <= 120 && health >= 80
);

const isDefenseValid = (defense) => (
    (defense == parseInt(defense)) && defense <= 10 && defense > 0
);

const isPrimitive = (value) => typeof(value) !== 'object';

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;