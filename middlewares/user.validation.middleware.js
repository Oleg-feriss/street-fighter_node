const { user } = require('../models/user');
const UserService = require('../services/userService');

const createUserValid = (req, res, next) => {
    const { id, ...rest } = req.body;
    
    if (id || !isModelComplete(rest) || !isBodyValid(rest)) {
        res.status(400);
        res.err = 'User entity to create is not valid';
    };

    next();
};

const updateUserValid = (req, res, next) => {
    const { id, ...rest} = req.body;
    
    if (id || !isBodyValid(rest) || isEmptyObject(rest)) {
        res.status(400);
        res.err = 'User entity to update is not valid';
    };

    next();
};

const isModelComplete = (data) => {
    for (const key in user) {
        if (key !== 'id' && !data[key]) {
            return false;
        }
    };
    return true;
};

const isBodyValid = (data) => {
    for (const key in data) {
        if (!isValid(key, data) || !isPrimitive(data[key])) {
            return false;
        }
    }
    return true;
};

const isEmptyObject = (data) => {
    for (const key in data) {
        if (data.hasOwnProperty(key)) {
            return false;
        }
    }
    return true;
}

const isValid = (keyType, data) => {
    switch(keyType) {
        case 'firstName': return data[keyType];
        case 'lastName': return data[keyType];
        case 'email': return isEmailValid(data[keyType]);
        case 'phoneNumber': return isPhoneNumberValid(data[keyType]);
        case 'password': return isPasswordValid(data[keyType]);
        default: return false;
    };
};

const isEmailValid = (email) => (
    email &&
    (typeof email === 'string' || email instanceof String) &&
    email.split('@')[1] === 'gmail.com' &&
    email.split('@').length === 2 &&
    isUnique(user => user.email === email)
);

const isUnique = (search) => {
    try {
        const user = UserService.getOneUser(search);
        return false;
    } catch (err) {
        return true;
    };
};

const isPhoneNumberValid = (phoneNumber) => (
    /^\+380\d{9}$/.test(phoneNumber) &&
    isUnique(user => user.phoneNumber === phoneNumber)
);

const isPasswordValid = (password) => password.length > 2;

const isPrimitive = (value) => typeof(value) !== 'object';

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;