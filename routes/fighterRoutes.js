const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    try {
        const content = FighterService.getFighters();
        res.data = content;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const { id } = req.params;
        const content = FighterService.getFighter(fighter => fighter.id === id);
        res.data = content;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
    if (!res.err) {
        try {
            const content = FighterService.createFighter(req.body);
            res.data = content;
        } catch (err) {
            res.err = err;
        }
    }
    next();
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    if (!res.err) {
        try {
            const { id } = req.params;
            const content = FighterService.updateFighter(id, req.body);
            res.data = content;
        } catch (err) {
            res.err = err;
        }
    }
    next();
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        const { id } = req.params;
        const content = FighterService.deleteFighter(id);
        res.data = content;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;