const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    try {
        const data = UserService.getAllUsers();
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const { id } = req.params;
        const data = UserService.getOneUser(user => user.id === id);
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
    if (!res.err) {
        try {
            const data = UserService.createUser(req.body);
            res.data = data;
        } catch (err) {
            res.err = err;
        }
    }
    next();
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
    if (!res.err) {
        try {
            const { id } = req.params;
            const data = UserService.updateUser(id, req.body);
            res.data = data;
        } catch (err) {
            res.err = err;
        }
    }
    next();
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        const { id } = req.params;
        const data = UserService.deleteUser(id);
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);


module.exports = router;